import java.sql.SQLException;

public class Customers {

	private String customer_firstName;
	private String customer_lastName;
	private String customer_address;
	private String customer_phone;
	Database myDB = null;

	
	/*public Customers() 
	{

		this.customer_firstName = null;
		this.customer_lastName = null;
		this.customer_address = null;
		this.customer_phone = null;
	}*/
	
	public Customers(String customer_firstName, String customer_lastName, String customer_address, String customer_phone, Database d) 
	{

		this.customer_firstName = customer_firstName;
		this.customer_lastName = customer_lastName;
		this.customer_address = customer_address;
		this.customer_phone = customer_phone;
		myDB = d;
	}

	public boolean insertCustomer() throws SQLException {
		
		boolean insert = myDB.addNewCustomer(customer_firstName, customer_lastName, customer_address, customer_phone);

		return insert;
	}

}
